import mods.charset.MaterialRegistry;

MaterialRegistry.registerRelation(<embers:brick_caminite>, "block", <embers:block_caminite_brick>);
MaterialRegistry.registerRelation(<embers:archaic_brick>, "block", <embers:archaic_tile>);
MaterialRegistry.registerRelation(<thebetweenlands:items_misc:10>, "block", <thebetweenlands:mud_bricks>);
MaterialRegistry.registerRelation(<pyrotech:material:5>, "block", <pyrotech:refractory_brick_block>);
MaterialRegistry.registerRelation(<pyrotech:material:16>, "block", <pyrotech:masonry_brick_block>);