import mods.soot.AlchemicalMixer;
import mods.soot.Still;
import mods.jei.JEI;
import crafttweaker.item.IItemStack;

val itemsToRemove = [
	<soot:stamp_text_raw>,
	<soot:stamp_text>,
] as IItemStack[];

for item in itemsToRemove {
	JEI.removeAndHide(item);
	item.removeAspects(allAspects);
}



recipes.remove(<soot:redstone_bin>);
scripts.utils.addShaped("redstone_bin", <soot:redstone_bin>, [
	[<ore:ingotSyrmorite>, null, <ore:ingotSyrmorite>], 
	[<ore:ingotSyrmorite>, null, <ore:ingotSyrmorite>], 
	[<ore:ingotSyrmorite>, <thebetweenlands:syrmorite_trapdoor>, <ore:ingotSyrmorite>]
]);

recipes.remove(<soot:scale>);
scripts.utils.addShaped("scale", <soot:scale>, [
	[<ore:ingotSilver>, <ore:dustRedstone>, <ore:ingotSilver>], 
	[null, <ore:ingotSilver>, null], 
	[null, <embers:block_caminite_brick_slab>, null]
]);

recipes.remove(<soot:alchemy_gauge>);
scripts.utils.addShaped("alchemy_gauge", <soot:alchemy_gauge>, [
	[<ore:dustRedstone>], 
	[<thebetweenlands:items_misc:32>], 
	[<ore:ingotAntimony>]
]);

recipes.remove(<soot:insulation>);
scripts.utils.addShaped("insulation", <soot:insulation>, [
	[<ore:plateSilver>], 
	[<ore:blockOctine>], 
	[<embers:ashen_tile>]
]);


AlchemicalMixer.remove(<liquid:antimony> * 12);

AlchemicalMixer.remove(<liquid:iron>);
AlchemicalMixer.remove(<liquid:gold>);
AlchemicalMixer.remove(<liquid:tin>);
AlchemicalMixer.remove(<liquid:copper>);
AlchemicalMixer.remove(<liquid:silver>);
AlchemicalMixer.remove(<liquid:syrmorite>);
AlchemicalMixer.remove(<liquid:octine>);

AlchemicalMixer.remove(<liquid:iron> * 4);
AlchemicalMixer.remove(<liquid:gold> * 4);
AlchemicalMixer.remove(<liquid:tin> * 4);
AlchemicalMixer.remove(<liquid:copper> * 4);
AlchemicalMixer.remove(<liquid:silver> * 4);
AlchemicalMixer.remove(<liquid:syrmorite> * 4);
AlchemicalMixer.remove(<liquid:octine> * 4);

AlchemicalMixer.add(<liquid:syrmorite> * 3, [<liquid:octine> * 4, <liquid:alchemical_redstone> * 3], {"dawnstone":4 to 8});
AlchemicalMixer.add(<liquid:lead> * 3, [<liquid:syrmorite> * 4, <liquid:alchemical_redstone> * 3], {"dawnstone":4 to 8});
AlchemicalMixer.add(<liquid:silver> * 3, [<liquid:lead> * 4, <liquid:alchemical_redstone> * 3], {"dawnstone":4 to 8});
AlchemicalMixer.add(<liquid:octine> * 3, [<liquid:silver> * 4, <liquid:alchemical_redstone> * 3], {"dawnstone":4 to 8});

AlchemicalMixer.add(<liquid:antimony> * 12, [<liquid:lead> * 8, <liquid:fluid_honey> * 4], {"dawnstone":16 to 32, "silver":16 to 24});
