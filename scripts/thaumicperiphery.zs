recipes.remove(<thaumicperiphery:pauldron>);
scripts.utils.addShaped("pauldron", <thaumicperiphery:pauldron>, [
	[null, <thaumcraft:plate:1>, <thaumcraft:plate:1>], 
	[<thaumcraft:plate:1>, <ore:ingotSyrmorite>, <ore:leather>], 
	[<ore:ingotSyrmorite>, null, <ore:ingotBrass>]
]);
