import mods.prodigytech.explosionfurnace.explosives;
import mods.prodigytech.explosionfurnace.dampeners;


recipes.remove(<prodigytech:energion_dust>);
recipes.remove(<patchouli:guide_book>.withTag({"patchouli:book": "prodigytech:guide"}));


mods.prodigytech.explosionfurnace.recipes.removeAll();

mods.prodigytech.explosionfurnace.recipes.addRecipe(<minecraft:iron_ingot> * 2, <prodigytech:ferramic_ingot> * 2, 360, <embers:blend_caminite>, 1);
mods.prodigytech.explosionfurnace.recipes.addRecipe(<minecraft:iron_ingot> * 8, <prodigytech:ferramic_ingot> * 8, 1440, <soot:caminite_clay>, 1);

explosives.remove(<prodigytech:sawdust>);
explosives.remove(<minecraft:gunpowder>);
explosives.add(<thebetweenlands:angry_pebble>, 360);
explosives.add(<thaumcraft:alumentum>, 720);

dampeners.remove(<prodigytech:ash>);
