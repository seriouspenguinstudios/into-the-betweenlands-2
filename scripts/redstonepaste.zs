recipes.remove(<redstonepaste:redstonepaste>);
scripts.utils.addShapeless("redstonepaste", <redstonepaste:redstonepaste> * 4, [<betweenlandsredstone:scabyst_dust>, <thebetweenlands:sap_spit>]);

recipes.remove(<redstonepaste:stickyrepeater>);
scripts.utils.addShapeless("stickyrepeater", <redstonepaste:stickyrepeater>, [<betweenlandsredstone:scabyst_repeater>, <thebetweenlands:sap_spit>]);

recipes.remove(<redstonepaste:stickycomparator>);
scripts.utils.addShapeless("stickycomparator", <redstonepaste:stickycomparator>, [<betweenlandsredstone:scabyst_comparator>, <thebetweenlands:sap_spit>]);

<redstonepaste:redstonepaste>.displayName = "Bluedust Paste";