recipes.remove(<naturescompass:naturescompass>);
scripts.utils.addShaped("naturescompass", <naturescompass:naturescompass>, [
	[<ore:treeSapling>, <ore:logWood>, <ore:treeSapling>],
	[<ore:logWood>, <thebetweenlands:dentrothyst_shard_green>, <ore:logWood>],
	[<ore:treeSapling>, <ore:logWood>, <ore:treeSapling>]
]);