import mods.jei.JEI;
import crafttweaker.item.IItemStack;
import crafttweaker.item.IIngredient;

/*
function addBrickRecipe(itemOut as IItemStack, itemIn as IIngredient) {
	recipes.remove(itemOut);
	scripts.utils.addShaped(itemOut, [
		[<contenttweaker:mortar>, itemIn, <contenttweaker:mortar>],
		[itemIn, <contenttweaker:mortar>, itemIn],
		[<contenttweaker:mortar>, itemIn, <contenttweaker:mortar>]
	]);		
}


addBrickRecipe(<thebetweenlands:betweenstone_bricks> * 4, <thebetweenlands:betweenstone>);
addBrickRecipe(<thebetweenlands:pitstone_bricks> * 4, <thebetweenlands:pitstone>);
addBrickRecipe(<thebetweenlands:cragrock_bricks> * 4, <thebetweenlands:cragrock>);
addBrickRecipe(<thebetweenlands:limestone_bricks> * 4, <thebetweenlands:limestone>);

addBrickRecipe(<thebetweenlands:mud_bricks>, <thebetweenlands:items_misc:10>);
addBrickRecipe(<pyrotech:refractory_brick_block>, <pyrotech:material:5>);


scripts.utils.addShapeless("lime_water_wood", <thebetweenlands:bl_bucket:0>.withTag({Fluid: {FluidName: "lime_water", Amount: 1000}}), [
	<thebetweenlands:bl_bucket:0>.withTag({Fluid: {FluidName: "swamp_water", Amount: 1000}}).marked("bucket"),
	<thebetweenlands:items_misc:27>,
	<thebetweenlands:items_misc:27>
],function(out,ins,cInfo) {
    return ins.bucket.withTag({Fluid: {FluidName: "lime_water", Amount: 1000}});
},null);

scripts.utils.addShapeless("lime_water_syrmorite", <thebetweenlands:bl_bucket:1>.withTag({Fluid: {FluidName: "lime_water", Amount: 1000}}), [
	<thebetweenlands:bl_bucket:1>.withTag({Fluid: {FluidName: "swamp_water", Amount: 1000}}).noReturn(),
	<thebetweenlands:items_misc:27>,
	<thebetweenlands:items_misc:27>
]);
*/