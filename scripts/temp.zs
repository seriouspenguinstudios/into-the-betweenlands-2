recipes.addShapeless("copper_ore_conversion", <thebetweenlands:octine_ore>, [<betweenores:copper_ore>]);
recipes.addShapeless("nickel_ore_conversion", <betweenores:lead_ore>, [<betweenores:nickel_ore>]);
recipes.addShapeless("aluminum_ore_conversion", <betweenores:silver_ore>, [<betweenores:aluminum_ore>]);

recipes.addShapeless("ingot_copper_conversion", <thebetweenlands:octine_ingot>, [<embers:ingot_copper>]);
recipes.addShapeless("ingot_aluminum_conversion", <embers:ingot_silver>, [<embers:ingot_aluminum>]);
recipes.addShapeless("ingot_nickel_conversion", <embers:ingot_lead>, [<embers:ingot_nickel>]);
