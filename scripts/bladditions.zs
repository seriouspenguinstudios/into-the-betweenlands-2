import crafttweaker.item.IItemStack;

scripts.utils.addInfoTooltip(<bladditions:corrupted_bone_wayfinder>.withTag({dimension: 21}), "Cragrock Tower Mirage");
scripts.utils.addInfoTooltip(<bladditions:corrupted_bone_wayfinder>.withTag({dimension: 22}), "Wight Fortress Mirage");
scripts.utils.addInfoTooltip(<bladditions:corrupted_bone_wayfinder>.withTag({dimension: 23}), "Sludgeon Mirage");

scripts.utils.addInfoTooltip(<bladditions:corrupted_bone_wayfinder>.withTag({dungeon_id: "maze_botania_start"}), "Dungeon: Botanical Gardens");
scripts.utils.addInfoTooltip(<bladditions:corrupted_bone_wayfinder>.withTag({dungeon_id: "maze_bloodmagic_start"}), "Dungeon: Sanguine Halls");
