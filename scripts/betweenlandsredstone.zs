<ore:dustRedstone>.add(<betweenlandsredstone:scabyst_dust>);

recipes.remove(<betweenlandsredstone:scabyst_dust>);
recipes.remove(<betweenlandsredstone:scabyst_block>);
recipes.remove(<betweenlandsredstone:white_pear_block>);
recipes.remove(<betweenlandsredstone:weedwood_chest_trapped>);
recipes.remove(<betweenlandsredstone:scabyst_tripwire>);
recipes.remove(<betweenlandsredstone:crafter>);

<betweenlandsredstone:scabyst_dust>.displayName = "Bluedust";
<betweenlandsredstone:scabyst_block>.displayName = "Bluedust Block";
<betweenlandsredstone:scabyst_torch>.displayName = "Bluedust Torch";
<betweenlandsredstone:scabyst_repeater>.displayName = "Bluedust Repeater";
<betweenlandsredstone:scabyst_comparator>.displayName = "Bluedust Comparator";
<betweenlandsredstone:scabyst_lamp>.displayName = "Bluedust Lamp";