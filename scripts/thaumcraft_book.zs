import crafttweaker.game.IGame;

game.setLocalization("got.dream", "You suddenly experience some sort of vision and quickly write it down before it fades.");

game.setLocalization("research.THEORYRESEARCH.title", "Inspirational Teas");

game.setLocalization("research.THEORYRESEARCH.stage.1", "For me to make any progress as a thaumaturge I cannot rely on simply studying the world around me or stumbling upon ancient knowledge. I will need to make my own discoveries and expand my knowledge through experimentation. Coming up with my own theories is the only way.<BR>I will need to create a relaxing and comfortable workspace to do my research. A nice hot cup of tea should do the trick.");

game.setLocalization("research.THEORYRESEARCH.stage.2", "Now that i have some tea, i can sit back and relax, and write down any discoveries i make as long as i have my thaumonomicon and scribing tools on hand.<BR>The different types of teas should help me focus on different types of discoveries, and each one of them should give me some Fundamental knowledge.<BR>I'm sure if i surround myself with inspiring objects, i can discover things even more quickly!");

game.setLocalization("research.Firebat.stage.1", "Pyrads are interesting beings. Despite being mostly made out of wood, they defend themselves with fire.<BR>This could potentially be useful in the future. ");

game.setLocalization("research.UNLOCKALCHEMY.stage.2", "My recent discoveries have given me a new insight into how the world is put together and I think I might know a way to transform matter by altering its Essentia.<BR>This will require experimentation and something to hold the alchemical substrate. A simple brewing stand will not do - what I need is a mystical crucible.<BR>A kettle is the right shape and size, but what is effectively a big metal bowl will never be able to hold the magical energies I wish to manipulate.<BR>Once again, applying some Salis Mundus to a kettle should resolve my problems.");

game.setLocalization("entity.Firebat.name", "Pyrads");
