#loader gamestagebooks
import mods.gamestagebooks.Book;
import mods.recipestages.Recipes;

Book.addBook("knowledge_of_druids", "Druidic Knowledge", "Book of Knowledge: Druids", "roots:pereskia", 0x4e8d22);
Book.addBook("knowledge_of_malice", "Malicious Knowledge", "Book of Knowledge: Malice", "embers:winding_gears", 0x786460);
Book.addBook("knowledge_of_decay", "Dreadful Knowledge", "Book of Knowledge: Decay", "thaumcraft:goggles", 0xdebd63);
Book.addBook("knowledge_of_technology", "Technological Knowledge", "Book of Knowledge: Technology", "immersiveengineering:powerpack", 0x354866);
