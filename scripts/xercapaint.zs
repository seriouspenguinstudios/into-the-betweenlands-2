recipes.remove(<xercapaint:item_canvas>);
recipes.addShaped("item_canvas", <xercapaint:item_canvas>, [
	[<ore:stickWood>, <ore:stickWood>, <ore:stickWood>], 
	[<ore:stickWood>, <thebetweenlands:items_misc:32>, <ore:stickWood>], 
	[<ore:stickWood>, <ore:stickWood>, <ore:stickWood>]
]);

recipes.addShaped("item_palette", <xercapaint:item_palette>, [
	[null, <ore:stickWood>, <ore:stickWood>],
	[<ore:stickWood>, <ore:leather>, <ore:stickWood>],
	[<ore:stickWood>, <ore:stickWood>, null]
]);