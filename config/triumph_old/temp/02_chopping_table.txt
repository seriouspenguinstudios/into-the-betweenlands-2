setTitle("Chopping Table")

setIcon(<pyrotech:chopping_block>)

setDescription("Craft a chopping table")

addParent("itbl2:chapter_1/01_getting_wood")

setRequiresParents()

//Criteria
criteria = addCriteria("hasChoppingBlock", "minecraft:inventory_changed")
criteria.addItem("pyrotech:chopping_block")

setPos(71,37)